package com.yunze;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

@SpringBootApplication
@MapperScan("com.yunze.iotapi.mapper")
@MapperScan("com.yunze.apiCommon.mapper")
@ServletComponentScan(basePackages = {"com.yunze.iotapi.interceptor"})
public class iotapiApplication {


    public static void main(String[] args) {

        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
        SpringApplication.run(iotapiApplication.class,args);

    }

}

