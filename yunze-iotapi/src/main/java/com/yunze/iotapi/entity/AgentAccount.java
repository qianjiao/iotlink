package com.yunze.iotapi.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@TableName(value = "yz_agent_account")
@Getter
@Setter
public class AgentAccount {

    @TableId(value = "id",type = IdType.ID_WORKER_STR)
    private String id;
    private String appId;
    private String password;
    private String accessKey;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
    private String agentName;
    private String agentId;
    private int times;
    private String openurl;
    private int typesys;



}
