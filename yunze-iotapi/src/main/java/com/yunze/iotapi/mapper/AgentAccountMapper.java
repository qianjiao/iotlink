package com.yunze.iotapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunze.iotapi.entity.AgentAccount;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AgentAccountMapper extends BaseMapper<AgentAccount> {

}
