package com.yunze.iotapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunze.iotapi.entity.AgentAccount;

public interface AgentAccountService extends IService<AgentAccount> {
}
