package com.yunze.iotapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunze.iotapi.entity.AgentAccount;
import com.yunze.iotapi.mapper.AgentAccountMapper;
import com.yunze.iotapi.service.AgentAccountService;
import org.springframework.stereotype.Service;

@Service
public class AgentAccountServiceImpl extends ServiceImpl<AgentAccountMapper, AgentAccount> implements AgentAccountService {
}
