package com.yunze.iotapi.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yunze.iotapi.entity.SysLogs;

/**
 * 系统日志
 */
public interface SysLogsService extends IService<SysLogs> {


}
