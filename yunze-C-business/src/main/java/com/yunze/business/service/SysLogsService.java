package com.yunze.business.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yunze.business.entity.SysLogs;

import java.util.List;

/**
 * 系统级日志 服务类
 */
public interface SysLogsService extends IService<SysLogs> {


}
